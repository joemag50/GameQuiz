
public class Configuraciones
{
	/**
	 * JCGE: Esta clase la vamos a usar para mantener las configuraciones
	 *       asi como la cantidad de vidas, los jugadores y la cantidad de preguntas
	 */
	
	public int cantidadPreguntas = Principal.p.cantidadPreguntas;
	public int vidas = 3;
	public int jugadores = 1;
	
	public void masVidas()
	{
		this.vidas++;
		if (this.vidas > 5)
		{
			this.vidas = 1;
		}
	}
	
	public void masJugadores()
	{
		this.jugadores++;
		if (this.jugadores > 4)
		{
			this.jugadores = 1;
		}
	}
}
