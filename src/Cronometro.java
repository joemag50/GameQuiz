import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;

public class Cronometro
{
	/**
	 * JCGE: Esta clase la usarmos para el timer
	 */
	
	Timer timer = new Timer();
	public int segundos = 0;
	public boolean frozen;
	
	// clase interna que representa una tarea, se puede crear varias tareas y asignarle al timer luego
	class MiTarea extends TimerTask
	{
		JLabel cron;
		
		MiTarea(JLabel lab)
		{
			cron = lab;
		}
		
		public void run()
		{
			segundos++;
			cron.setText("Time: "+segundos);
		}
	}
	
	public void Start(JLabel lab) throws Exception
	{
		frozen = false;
		// le asignamos una tarea al timer
		timer.schedule(new MiTarea(lab), 0, 1000);
	}
	
	public int Stop()
	{
		frozen = true;
		int res = this.segundos;
		this.segundos = 0;
		return res;
	}
}
