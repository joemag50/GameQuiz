import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Jugador
{
	/**
	 * JCGE: Esta clase es lo que define al jugador
	 */
	
	public String nombre ;
	public int puntos, vidas, vidasPerdidas = 0;
	public ArrayList<JPanel> sprVidas;
	public boolean yaPerdio;
	
	public JPanel cuadro;
	public JLabel lblnombre, lblpuntos;
	
	Jugador(String name, int points, int lifes)
	{
		this.nombre = name;
		this.puntos = points;
		this.vidas = lifes;
		this.yaPerdio = false;
	}
	
	public void vaciarPanel()
	{
		if (cuadro != null)
		{
			cuadro.setVisible(false);
			cuadro.removeAll();
		}
	}
	
	public void vaciarVidas()
	{
		if (sprVidas != null)
		{
			for (JPanel jp: sprVidas)
			{
				jp.setVisible(false);
				jp.removeAll();
			}
		}
	}
	
	public JPanel dibujaPanel(Jugador player, int x)
	{
		cuadro = new JPanel();
		cuadro.setBounds(x, 700, 150, 100);
		cuadro.setLayout(null);
		
		lblnombre = new JLabel("Player: "+ player.nombre);
		lblnombre.setBounds(10, 0, 100, 40);
		lblnombre.setFont(new Font("Sans", Font.BOLD, 17));
		
		lblpuntos = new JLabel(String.format("Score: %s/%s", this.puntos, Principal.conf.cantidadPreguntas));
		lblpuntos.setFont(new Font("Sans", Font.BOLD, 17));
		lblpuntos.setBounds(10, 30, 100, 30);
		
		this.dibujaVidas(player, cuadro);
		
		cuadro.add(lblnombre);
		cuadro.add(lblpuntos);
		return cuadro;
	}
	
	public void dibujaVidas(Jugador player, JPanel panel)
	{
		this.vaciarVidas();
		sprVidas = new ArrayList<JPanel>();
		for (int i = 0; i<=Principal.conf.vidas-1; i++)
		{
			sprVidas.add(new JPanel());
		}
		int x = 15, i = this.vidasPerdidas;
		for (JPanel jp: sprVidas)
		{
			jp.setBackground(Color.GREEN);
			if (i > 0)
			{
				i--;
				jp.setBackground(Color.RED);
			}
			jp.setBounds(x, 70, 15, 15); x += 20;
			jp.setVisible(true);
			panel.add(jp);
		}
	}
	
	public void select()
	{
		this.cuadro.setBackground(VentanaPrincipal.colores.get(0));
		this.lblnombre.setForeground(VentanaPrincipal.colores.get(2));
		this.lblpuntos.setForeground(VentanaPrincipal.colores.get(2));
	}
	
	public void deSelect()
	{
		this.cuadro.setBackground(VentanaPrincipal.colores.get(2));
		this.lblnombre.setForeground(Color.BLACK);
		this.lblpuntos.setForeground(Color.BLACK);
	}
	
	public void masPunto()
	{
		this.puntos++;
		lblpuntos.setText(String.format("Score: %s/%s", this.puntos, Principal.conf.cantidadPreguntas));
	}
	
	public void menosVida()
	{
		this.sprVidas.get(this.vidasPerdidas).setBackground(Color.RED);
		this.vidasPerdidas++;
	}
}
