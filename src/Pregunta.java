import java.util.ArrayList;
import java.util.Collections;

public class Pregunta
{
	private String pregunta;
	private String[] respuestas;
	private int respuestaCorrecta;
	private int cantidadPreguntas;
	private int tope;
	private int[] secuencia;
	
	Pregunta()
	{
		this.cantidadPreguntas = Principal.conf.cantidadPreguntas;
		this.tope = 0 ;
		this.secuencia = this.nuevaSecuencia(this.cantidadPreguntas);
	}
	
	public void nuevaPregunta()
	{
		Principal.p.setPregunta(secuencia[tope]);
		
		this.pregunta = Principal.p.pregunta;
		this.respuestas = Principal.p.respuestas;
		this.respuestaCorrecta = Principal.p.respuestaCorrectaInt;
		
		this.tope++;
	}
	
	public boolean existenPreguntas()
	{
		if (this.tope == this.cantidadPreguntas)
			return false;
		return true;
	}
	
	public String getPregunta()
	{
		return this.pregunta;
	}
	
	public String[] getRespuestas()
	{
		return this.respuestas;
	}
	
	public int getRespuestaCorrecta()
	{
		return this.respuestaCorrecta;
	}
	
	public int getCantidadPreguntas()
	{
		return this.cantidadPreguntas;
	}
	//JCGE: Nueva secuencia aleatoria
	public int[] nuevaSecuencia(int tamano)
	{
		ArrayList<Integer> secuenciaBase = new ArrayList<Integer>();
		for (int i = 0; i<tamano; i++)
		{
			secuenciaBase.add(i);
		}
		Collections.shuffle(secuenciaBase);
		int[] sec = new int[tamano];
		for (int i = 0; i<tamano; i++)
		{
			sec[i] = secuenciaBase.get(i).intValue();
		}
		return sec;
	}
}
