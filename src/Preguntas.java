import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Preguntas
{
	private ArrayList<String> preguntas;
	private ArrayList<String[]> respuestass;
	private ArrayList<Integer> respuestasCorrectasInt;
	
	public String pregunta;
	public String[] respuestas;
	public int respuestaCorrectaInt;
	
	public int cantidadPreguntas = 0;
	
	String nombreArchivo = "qna.joe", linea = null;
	
	Preguntas()
	{
		//JCGE: Declaramos las preguntas y respuestas
		this.readFile();
	}
	
	public void readFile()
	{
		try
		{
			// Creamos un objeto para leer el archivo
			FileReader fileReader = new FileReader(this.nombreArchivo);
			// Ponemos en memoria el archivo
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			//Instanciamos los objetos que queremos rellenar
			preguntas = new ArrayList<String>();
			respuestass = new ArrayList<String[]>();
			respuestasCorrectasInt = new ArrayList<Integer>();
			
			while((linea = bufferedReader.readLine()) != null)
			{
				String[] lineas = linea.split(";");
				preguntas.add(lineas[0]);
				respuestass.add(lineas[1].split(","));
				respuestasCorrectasInt.add(Integer.parseInt(lineas[2]));
				this.cantidadPreguntas++;
			}
			
			//Cerramos el archivo, basicamente para que no se quede en la memoria
			bufferedReader.close();
		}
		catch(FileNotFoundException ex)
		{
			JOptionPane.showMessageDialog(null, "Unable to open file '" + nombreArchivo + "'");
			System.out.println("Unable to open file '" + nombreArchivo + "'");
			//Nos salimos del programa
			System.exit(0);
			Principal.vent.dispose();
		}
		catch(IOException ex)
		{
			JOptionPane.showMessageDialog(null, "Error reading file '" + nombreArchivo + "'");
			System.out.println("Error reading file '" + nombreArchivo + "'");
			//Nos salimos del programa
			System.exit(0);
			Principal.vent.dispose();
		}
	}
	
	public void setPregunta(int pregunta)
	{
		this.pregunta = this.preguntas.get(pregunta);
		this.respuestas = this.respuestass.get(pregunta);
		this.respuestaCorrectaInt = this.respuestasCorrectasInt.get(pregunta);
	}
}
