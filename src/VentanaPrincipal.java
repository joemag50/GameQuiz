import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

//Heredamos de JFrame para sacar de la libreria swing
public class VentanaPrincipal extends JFrame
{
	/**
	 * JCGE: Clase para la ventana generica
	 */
	private static final long serialVersionUID = -1171527061718987226L;
	//JCGE: Medidas de la pantalla
	static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	static final Double WIDTH = new Double(screenSize.getWidth());
	static final Double HEIGHT = new Double(screenSize.getHeight());
	
	static final Integer WIDTHint = WIDTH.intValue();
	static final Integer HEIGHTint = HEIGHT.intValue();
	
	protected Container frame;
	protected JPanel panelSur, panelNorte;
	protected JLabel elTiempo,l_titulo;
	
	public static ArrayList<Color> colores;
	public static JPanel panelCentro;
	//JCGE: Constructor aca mamalon
	VentanaPrincipal ()
	{
		//JCGE: Preparamos el marco principal
		frame = getContentPane();
		frame.setLayout(new BorderLayout());
		//JCGE: Preparamos la ventana
		this.setTitle("Game Quiz");
		this.setExtendedState(MAXIMIZED_BOTH);
		this.setResizable(false);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		//JCGE: Preparamos las areas de trabajo de las ventanas
		panelSur    = new JPanel();
		panelCentro = new JPanel();
		panelNorte = new JPanel();
		panelNorte.setLayout(null);
		
		//JCGE: Preparamos la paleta de colores
		colores = new ArrayList<Color>();
		colores.add(new Color(41,44,68));
		colores.add(new Color(255,83,73));
		colores.add(new Color(240,240,241));
		colores.add(new Color(24,205,202));
		colores.add(new Color(79,128,255));
		
		panelNorte.setBackground(colores.get(0));
		
		//JCGE: Estas instancias las usamos para ver la fecha
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
		Calendar currentCalendar = Calendar.getInstance();
		Date currentTime = currentCalendar.getTime();
		
		//JCGE: Paneles de la parte inferior de la pantalla
		JPanel statusPanel = new JPanel();
		JPanel statusRightPanel = new JPanel();
		
		//Les asignamos un borde para identificarlo
		statusPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusRightPanel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		
		//Agregamos los paneles nuevos al panel fijo del sur
		panelSur.add(statusPanel, BorderLayout.SOUTH);
		panelSur.add(statusRightPanel, BorderLayout.SOUTH);
		
		//Les asignamos un tamaño
		statusPanel.setPreferredSize(new Dimension(500, 20));
		statusRightPanel.setPreferredSize(new Dimension(500, 20));
		
		//Les asignamos una plantilla de acomodo
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		statusRightPanel.setLayout(new BoxLayout(statusRightPanel, BoxLayout.X_AXIS));
		
		//Etiquetas con la informacion
		elTiempo = new JLabel("Fecha: " + dateFormat.format(currentTime));
		l_titulo = new JLabel("Bienvenido");
		//Agregamos las etiquetas a los paneles con borde
		statusRightPanel.add(l_titulo);
		statusPanel.add(elTiempo);
		
		//Los paneles con las etiquetas van a dentro del panel sur
		panelSur.add(statusRightPanel);
		panelSur.add(statusPanel);
	}
	public void finGUI(JPanel panelc)
	{
		//JCGE
		frame.remove(panelCentro);
		panelCentro = panelc;
		frame.add(panelSur,BorderLayout.SOUTH);
		frame.add(panelNorte, BorderLayout.NORTH);
		frame.add(panelCentro,BorderLayout.CENTER);
		this.setLocationRelativeTo(null);
		pack();
	}
}
