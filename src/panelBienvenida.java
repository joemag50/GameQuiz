import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class panelBienvenida extends panelBase implements ActionListener
{
	/**
	 * JCGE: Este es el panel donde le damos la bienvenida
	 * Sugiero que deberiamos poner aqui el menu de niveles y tópicos
	 */
	private static final long serialVersionUID = 1L;
	
	protected JButton inicio, salir;
	protected JLabel titulo;
	protected JPanel pTitulo;
	private int x = ((VentanaPrincipal.WIDTHint/2)-250 ), y = 600, width = 200, height = 40;
	
	panelBienvenida()
	{
		pTitulo = new JPanel();
		pTitulo.setLayout(null);
		pTitulo.setBounds(0, 100, VentanaPrincipal.WIDTHint, 100);
		pTitulo.setBackground(VentanaPrincipal.colores.get(1));
		
		titulo = new JLabel("Welcome to Game Quiz", SwingConstants.CENTER);
		titulo.setBounds(0, 0, VentanaPrincipal.WIDTHint, 100);
		titulo.setVisible(true);
		titulo.setFont(new Font("Sans", Font.BOLD, 40));
		
		inicio = new JButton("Start");
		inicio.addActionListener(this);
		inicio.setVisible(true);
		inicio.setBounds(x, y, width, height); x+=300;
		inicio.setBackground(VentanaPrincipal.colores.get(2));
		
		salir = new JButton("Exit");
		salir.addActionListener(this);
		salir.setVisible(true);
		salir.setBounds(x, y, width, height);
		salir.setBackground(VentanaPrincipal.colores.get(2));
		
		pTitulo.add(titulo);
		this.add(pTitulo);
		this.add(inicio);
		this.add(salir);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		//JCGE: botones
		String boton = arg0.getActionCommand();
		if (boton == "Start")
		{
			panelJuego pj = new panelJuego();
			Principal.vent.finGUI(pj);
		}
		
		if (boton == "Exit")
		{
			System.exit(0);
			Principal.vent.dispose();
		}
	}
}
