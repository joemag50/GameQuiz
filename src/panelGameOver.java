import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class panelGameOver extends panelBase implements ActionListener
{
	/**
	 * JCGE: Cuando pierdan o cuando se acaben las preguntas, vamos a mostrar este panel
	 *       Tomamos el arraylist de los jugadores, para mostrar los puntajes en este panel
	 */
	private static final long serialVersionUID = 1L;
	
	private JLabel titulo, time;
	private JButton menu;
	private JPanel pGameOver;
	
	panelGameOver(ArrayList<Jugador> jugadores, int timeFinish)
	{
		pGameOver = new JPanel();
		pGameOver.setLayout(null);
		pGameOver.setVisible(true);
		pGameOver.setBounds(0, 100, VentanaPrincipal.WIDTHint, 100);
		pGameOver.setBackground(VentanaPrincipal.colores.get(1));
		
		titulo = new JLabel("Game Over", SwingConstants.CENTER);
		titulo.setBounds(0, 0, VentanaPrincipal.WIDTHint, 100);
		titulo.setVisible(true);
		titulo.setFont(new Font("Sans", Font.BOLD, 40));
		
		time = new JLabel("Time: "+timeFinish, SwingConstants.CENTER);
		time.setFont(new Font("Sans", Font.BOLD, 23));
		time.setBounds(0, 350, VentanaPrincipal.WIDTHint, 30);
		
		menu = new JButton("Menu");
		menu.addActionListener(this);
		menu.setBackground(VentanaPrincipal.colores.get(2));
		menu.setBounds((VentanaPrincipal.WIDTHint/2)-100, 500, 200, 70);
		
		int x = 50;
		for (Jugador j: jugadores)
		{
			this.add(j.dibujaPanel(j, x));
			x+=200;
		}
		
		pGameOver.add(titulo);
		this.add(pGameOver);
		this.add(menu);
		this.add(time);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		//JCGE
		String boton = arg0.getActionCommand();
		if (boton == "Menu")
		{
			panelBienvenida pb = new panelBienvenida();
			Principal.vent.finGUI(pb);
		}
	}
}
