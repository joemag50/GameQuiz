import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class panelJuego extends panelBase implements ActionListener
{
	/**
	 * JCGE: Panel donde toda la magia del juego ocurre
	 */
	private static final long serialVersionUID = 1L;
	
	protected ArrayList<JButton> respuestas;
	protected ArrayList<JPanel> vidas;
	protected ArrayList<Jugador> jugadores;
	protected JPanel pPregunta;
	protected JLabel pregunta, puntaje, crono, lbljugador;
	protected Pregunta p;
	protected Cronometro c;
	
	private JButton iniciar, cuantasVidas, cuantosJugadores;
	private int x = 300, y = 300, width = 480, height = 150, turno;
	
	panelJuego()
	{
		p = new Pregunta();
		c = new Cronometro();
		
		crono = new JLabel("Time: 0", SwingConstants.CENTER);
		crono.setBounds(0,0,VentanaPrincipal.WIDTHint, 100);
		crono.setFont(new Font("Sans", Font.BOLD, 23));
		
		this.creaJugadores();
		
		pPregunta = new JPanel();
		pPregunta.setLayout(null);
		pPregunta.setVisible(true);
		pPregunta.setBounds(0, 100, VentanaPrincipal.WIDTHint, 100);
		pPregunta.setBackground(VentanaPrincipal.colores.get(1));
		
		pregunta = new JLabel("Answer correctly the following questions", SwingConstants.CENTER);
		pregunta.setBounds(0, 0, VentanaPrincipal.WIDTHint, 100);
		pregunta.setFont(new Font("Sans", Font.BOLD, 40));
		
		cuantosJugadores = new JButton("Players: "+Principal.conf.jugadores);
		cuantosJugadores.setBounds((VentanaPrincipal.WIDTHint/2)-100, 500, 200, 40);
		cuantosJugadores.setVisible(true);
		cuantosJugadores.addActionListener(this);
		cuantosJugadores.setBackground(VentanaPrincipal.colores.get(2));
		cuantosJugadores.setActionCommand("jugadores");
		
		cuantasVidas = new JButton("Lifes: "+Principal.conf.vidas);
		cuantasVidas.setBounds((VentanaPrincipal.WIDTHint/2)-100, 550, 200, 40);
		cuantasVidas.setVisible(true);
		cuantasVidas.addActionListener(this);
		cuantasVidas.setBackground(VentanaPrincipal.colores.get(2));
		cuantasVidas.setActionCommand("vidas");
		
		iniciar = new JButton("Start");
		iniciar.setBounds((VentanaPrincipal.WIDTHint/2)-100, 600, 200, 40);
		iniciar.setVisible(true);
		iniciar.addActionListener(this);
		iniciar.setBackground(VentanaPrincipal.colores.get(2));
		
		pPregunta.add(pregunta);
		this.add(iniciar);
		this.add(pPregunta);
		this.add(crono);
		this.add(cuantasVidas);
		this.add(cuantosJugadores);
	}
	
	public void creaVidas()
	{
		for (Jugador j: jugadores)
		{
			j.dibujaVidas(j, j.cuadro);
		}
		repaint();
	}
	
	public void creaJugadores()
	{
		if (this.jugadores != null)
		{
			for (Jugador j: this.jugadores)
			{
				j.vaciarPanel();
			}
			this.jugadores.clear();
		}
		this.jugadores = new ArrayList<Jugador>();
		for (int i = 0; i <= Principal.conf.jugadores-1; i++)
		{
			jugadores.add(new Jugador(""+(i+1), 0, Principal.conf.vidas));
		}
		int x = 50;
		for (Jugador j: jugadores)
		{
			this.add(j.dibujaPanel(j, x));
			x+=200;
		}
		repaint();
	}
	//JCGE: con esto ya iniciamos
	public void iniciaRonda()
	{
		pregunta.setFont(new Font("Sans", Font.BOLD, 25));
		//lbljugador.setText("Player: "+this.jugador);
		respuestas = new ArrayList<JButton>();
		
		for (int i = 0; i <= 3; i++)
		{
			respuestas.add(new JButton());
			respuestas.get(respuestas.size()-1).setActionCommand(""+i);
		}
		
		int j = 0;
		for (JButton jb: respuestas)
		{
			jb.addActionListener(this);
			jb.setBounds(x, y, width, height);
			jb.setBackground(VentanaPrincipal.colores.get(2));
			jb.setFont(new Font("Sans", Font.BOLD, 25));
			y+=180;
			this.add(jb);
			j++;
			if (j == 2)
			{
				y= 300;
				x+=500;
			}
		}
		this.turno = Principal.conf.jugadores;
		this.nuevaPregunta();
		this.repaint();
	}
	
	public void siguienteTurno()
	{
		this.turno++;
		if (this.turno > Principal.conf.jugadores-1)
		{
			this.turno = 0;
		}
	}
	
	public void nuevaPregunta()
	{
		p.nuevaPregunta();
		pregunta.setText(p.getPregunta());
		
		do 
		{
			this.siguienteTurno();
			System.out.println(""+this.turno);
		}
		while (this.jugadores.get(this.turno).yaPerdio);
		
		int i = 0;
		for(JButton jb: respuestas)
		{
			jb.setText(p.getRespuestas()[i]);
			jb.setActionCommand(""+i);
			i++;
		}
		
		for (Jugador j: this.jugadores)
		{
			j.deSelect();
		}
		this.jugadores.get(this.turno).select();
	}
	
	public boolean evaluacion(int respuesta)
	{
		if (respuesta == this.p.getRespuestaCorrecta())
		{
			return true;
		}
		return false;
	}
	

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		//JCGE: botones
		String boton = arg0.getActionCommand();
		if (boton == "jugadores")
		{
			Principal.conf.masJugadores();
			cuantosJugadores.setText("Players: "+Principal.conf.jugadores);
			this.creaJugadores();
			return;
		}
		if (boton == "vidas")
		{
			Principal.conf.masVidas();
			cuantasVidas.setText("Lifes: "+Principal.conf.vidas);
			this.creaVidas();
			return;
		}
		if (boton == "Start")
		{
			this.iniciaRonda();
			iniciar.setVisible(false);
			cuantasVidas.setVisible(false);
			cuantosJugadores.setVisible(false);
			try {
				c.Start(crono);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		
		int respuesta = Integer.parseInt(boton);
		if (this.evaluacion(respuesta))
		{
			this.jugadores.get(this.turno).masPunto();
		}
		else
		{
			this.jugadores.get(this.turno).menosVida();
		}
		
		if (!p.existenPreguntas())
		{
			panelGameOver pj = new panelGameOver(jugadores, c.Stop());
			Principal.vent.finGUI(pj);
			return;
		}
		
		if (this.jugadores.get(this.turno).vidasPerdidas == Principal.conf.vidas)
		{
			this.jugadores.get(this.turno).yaPerdio = true;
		}
		this.nuevaPregunta();
	}
}
